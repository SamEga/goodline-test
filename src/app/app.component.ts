import { Component } from '@angular/core';
import { DataService } from './data';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  private data: Array<object>;

  constructor(private dataService: DataService) {
    this.data = this.dataService.getData();
  }
}
